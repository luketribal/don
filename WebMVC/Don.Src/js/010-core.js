let TRIBAL = {
	/**
	 * debounce()
	 * Taken from _underscore.js
	 *
	 * @param	{function}	func 		Function to be executed
	 * @param	{number} 	wait 		Wait for next iteration for n in milliseconds
	 * @return	{boolean}	immediate	Trigger the function on the leading edge [true], instead of the trailing [false]
	 */
	debounce: function Debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this;
			var args = arguments;

			var later = function() {
				timeout = null;

				if(!immediate) {
					func.apply(context, args);
				}
			};

			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);

			if(callNow) {
				func.apply(context, args);
			}
		};
	},
};