/*! registerform */
/***************************************************************************************************************************************************************
 *
 * register form
 *
 * 
 *
 **************************************************************************************************************************************************************/


(function (TRIBAL) {
    var module = {};
    var $region = $('.region-banner');


    /**
	 * init()
	 * Contructor function for this module
	 */
    module.init = function () {
        var self = this;
        //toggle the file upload visibility based on the browser
        if(Function('/*@cc_on return document.documentMode===10@*/')()){
            $('#btnUploadFile').hide()
            $('#btnFileUpload').show()
        }else{
            $('#btnUploadFile').show()
            $('#btnFileUpload').hide()
        }
        // extend jquery range validator to work for required checkboxes
        var defaultRangeValidator = $.validator.methods.range;
        $.validator.methods.range = function (value, element, param) {
            if (element.type === 'checkbox') {
                // if it's a checkbox return true if it is checked
                return element.checked;
            } else {
                // otherwise run the default validation function
                return defaultRangeValidator.call(this, value, element, param);
            }
        }

        $(document)
            .ready(function () {

                var input = $('.input-validation-error:first');

                if (input != undefined && input.length > 0) {
                    input.focus();
                } else {
                    var message = $('.field-validation-error:first');
                    if (message != undefined && message.length > 0) $(window).scrollTop($(message).offset().top);
                }


                self.helpers.showParentConsent();
                if ($("#pnlParentConsent").is(":visible")) {
                    $('#chbParentConsent').prop('checked', true);
                }

                $('.toggle-image')
                    .mouseover(function () {
                        $(this).children('img').first().attr('src', '/assets/images/' + $(this).data('mouse-over'));
                    })
                    .mouseout(function () {
                        $(this).children('img').first().attr('src', '/assets/images/' + $(this).data('mouse-out'));
                    });
                $('a[id*=btnFacebook]')
                    .click(function (e) {
                        e.preventDefault();
                        FB.login(function (response) {
                            if (response.authResponse != undefined) {
                                FB.api('/me',
                                    { locale: 'en_AU', fields: 'first_name, last_name, email' },
                                    function (response) {
                                        $('#pnlSignUp').hide();
                                        $('#txtFirstname').val(response.first_name);
                                        $('#txtLastname').val(response.last_name);
                                        $('#txtEmail').val(response.email);
                                    }
                                );
                            }
                        },
                            { scope: 'email' });
                    });

                $('#btnFileUpload')
                    .change(function () {
                        self.helpers.readURL(this, document.getElementById('imgPreview'));
                    });

                $("#txtReason")
                    .on('keyup',
                        function () {
                            var words = this.value.match(/\S+/g);
                            if (words != null && words.length > 120) {
                                // Split the string on first 120 words and rejoin on spaces
                                var trimmed = $(this).val().split(/\s+/, 120).join(" ");
                                // Add a space at the end to keep new typing making new words
                                $(this).val(trimmed + " ");
                            }
                        });

                $('#drpDobMonth')
                    .change(function () {
                        self.helpers.refreshDaysDropdown();
                        self.helpers.showParentConsent();
                    });

                $('#drpDobYear')
                    .change(function () {
                        var month = $('#drpDobMonth').val();

                        //only refresh dropdown when
                        //Month is Feb
                        if (month == 2) {
                            self.helpers.refreshDaysDropdown();
                        }

                        self.helpers.showParentConsent();
                    });

                $('#drpDobDay')
                    .change(function () {
                        self.helpers.showParentConsent();
                    });

                $('#btnUploadFile')
                    .click(function (e) {
                        e.preventDefault();
                        $('#btnFileUpload').trigger('click');
                    });

                // Instantiate the Typeahead UI
                $('#txtAddress').typeahead(null, {
                    display: 'value',
                    source: self.helpers.initializeSearch()
                });

                $('a[id*=btnSubmit]')
                    .click(function (e) {
                        e.preventDefault();
                        $('form').submit();
                    });

                if ($("form").data("validator") != undefined)
                    $("form").data("validator").settings.ignore = 'input.tt-hint, :hidden, .ignore';
            });
    };

    module.helpers = {
        readURL: function (input, img) {
            $('#lblFileUploadResult').hide();
            if (input.files && input.files[0]) {
                $('#lblFilename').text(input.files[0].name);
                $('#lblFileUploadResult').show();

                var reader = new FileReader();

                reader.onload = function (e) {
                    $(img).attr('src', e.target.result);
                    $(img).show();
                }

                reader.readAsDataURL(input.files[0]);
            }
        },
        getDaysInMonth: function (month, year) {
            var date = new Date(year, --month, 1);
            var days = [];
            while (date.getMonth() == month) {
                days.push(new Date(date));
                date.setDate(date.getDate() + 1);
            }
            return days;
        },

        refreshDaysDropdown: function () {
            var year = $('#drpDobYear').val() || new Date().getFullYear();
            var month = $('#drpDobMonth').val() || 1;
            var selectedDay = $('#drpDobDay').val();
            $('#drpDobDay').empty();

            $("<option />",
                {
                    val: '',
                    text: 'Day'
                })
                .appendTo($('#drpDobDay'));

            this.getDaysInMonth(month, year)
                .forEach(function (day) {
                    $("<option />",
                        {
                            val: day.getDate(),
                            text: day.getDate(),
                            selected: day.getDate() == selectedDay
                        })
                        .appendTo($('#drpDobDay'));
                });
        },
        showParentConsent: function () {
            var result = false;
            var year = parseInt($('#drpDobYear').val());
            var month = parseInt($('#drpDobMonth').val());
            var selectedDay = parseInt($('#drpDobDay').val());

            //year, month, day must be selected in order to show parental consent
            if (isNaN(year) || isNaN(month) || isNaN(selectedDay)) {
                result = false;
            } else {
                var birthday = new Date(year, --month, selectedDay);
                var today = new Date();
                var age = today.getFullYear() - birthday.getFullYear();
                var m = today.getMonth() - birthday.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthday.getDate())) {
                    age--;
                }
                result = age < 18;
            }

            $('#chbParentConsent').prop('checked', !result);
            result ? $('#pnlParentConsent').show() : $('#pnlParentConsent').hide();
        },
        initializeSearch: function () {
            // Instantiate the Bloodhound suggestion engine
            var postcodes = new Bloodhound({
                datumTokenizer: function (datum) {
                    return Bloodhound.tokenizers.whitespace(datum.value);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    wildcard: '%QUERY',
                    url: '/api/values/SearchPostcode?query=%QUERY',
                    transform: function (response) {
                        if (response != undefined) {
                            // Map the remote source JSON array to a JavaScript object array
                            return $.map(response,
                                function (result) {
                                    return {
                                        value: result.location + ', ' + result.state + ' ' + result.postcode
                                    };
                                });
                        }
                    }
                }
            });

            return postcodes;
        }
    }

    TRIBAL.registerform = module;
    // run module
    TRIBAL.registerform.init();

}(TRIBAL));