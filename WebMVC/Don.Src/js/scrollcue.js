/*! scrollcue */
/***************************************************************************************************************************************************************
 *
 * scrollcue
 *
 * 
 *
 **************************************************************************************************************************************************************/


(function(TRIBAL) {
	var module = {};
	var $region = $('.region-banner');
	

	/**
	 * init()
	 * Contructor function for this module
	 */
	module.init = function() {
		if(!$region.length) return;

		
		$('.banner__scroll-cue').on('click', scrollCueClick);

		
	};

	var scrollCueClick = function() {
		var $next = $region.nextAll('.region:eq(0)').first();		
		$('html, body').animate({
			scrollTop: $next.offset().top
		}, 750, 'easeOutQuint');
	};

	
	

	TRIBAL.bannerscrollcue = module;
	// run module
	TRIBAL.bannerscrollcue.init();

}(TRIBAL));