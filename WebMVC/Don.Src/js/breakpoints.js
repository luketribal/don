/*! Breakpoints v1.0.0 */
/***************************************************************************************************************************************************************
 *
 * breakpoints
 *
 * Enables you to query the current breakpoint
 *
 **************************************************************************************************************************************************************/


(function(TRIBAL) {
	var module = {};
	var breakpoint = false;
	var $detector = $('#breakpoint-detection');
	var $window = $(window);

	/**
	 * init()
	 * Contructor function for this module
	 */
	module.init = function() {
		if(!$detector.length) return;

		update();

		$window.on('resize', resize);
		$window.on('resizeComplete', update);
	};


	/**
	 * resize()
	 * Debounces the resize event
	 * Triggers an event when completed
	 */
	var resize = TRIBAL.debounce(function() {
			$window.trigger('resizeComplete');
		}, 300);


	/**
	 * update()
	 * Set the breakpoint as current
	 */
	var update = function() {
		var current = $detector.find('> div:visible');

		if(!current.length)
			return false;

		current = current.attr('data-breakpoint');

		if(typeof(breakpoint) !== 'undefined' && breakpoint !== current) {
			$window.trigger('breakpointChanged', current);
		}

		breakpoint = current;
	};


	/**
	 * get()
	 * Return the breakpoint
	 *
	 * @return {string} The breakpoint
	 */
	module.get = function() {
		return breakpoint;
	};


	/**
	 * is()
	 * Check if is the breakpoint passed
	 *
	 * @param {string} bp The breakpoint to lookup
	 * @return {boolean} Is that breakpoint active?
	 */
	module.is = function(bp) {
		if(typeof(bp) !== 'undefined' && bp.toLowerCase() ===  breakpoint) 
			return true;
		else
			return false;
	};


	TRIBAL.breakpoints = module;
	// run module
	TRIBAL.breakpoints.init();

}(TRIBAL));