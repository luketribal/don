/*! share popver */
/***************************************************************************************************************************************************************
 *
 * share popover
 *
 * 
 *
 **************************************************************************************************************************************************************/


(function (TRIBAL) {
    var module = {};
    var $region = $('.region-banner');


    /**
	 * init()
	 * Contructor function for this module
	 */
    module.init = function () {
        $(document)
            .ready(function () {
                $('.share-container')
                    .popover({
                        placement: 'bottom',
                        trigger: 'click focus',
                        content: '<nav class="popover__navSocial">' +
                            '<ul class="popover__nav-items">' +
                            '<li class="popover__nav-socialitem">' +
                            '<a class="share-nav__link share-nav__link--facebook" href="https://www.facebook.com/sharer/sharer.php?u=http://www.australiandons.com.au?fbrefresh=497" target="_blank"></a>' +
                            '</li>' +
                            '<li class="popover__nav-socialitem">' +
                            '<a class="share-nav__link share-nav__link--twitter" href="https://twitter.com/intent/tweet?text=Do%20you%20go%20by%20the%20name%20Don,%20Donna%20or%20‘The%20Don’?%20If%20so,%20you%20could%20be%20DON%20Smallgoods%20next%20spokesperson.%20Sign%20up%20at%20www.australiandons.com.au" target="_blank"></a>' +
                            '</li>' +
                            '<li class="popover__nav-socialitem">' +
                            '<a class="share-nav__link share-nav__link--pinterest" href="https://pinterest.com/pin/create/button/?url=http://www.australiandons.com.au&media=http://www.australiandons.com.au/assets/images/EDM-header_image.jpg&description=Do%20you%20go%20by%20the%20name%20Don,%20Donna%20or%20%E2%80%98The%20Don%E2%80%99?%20If%20so,%20you%20could%20be%20DON%20Smallgoods%20next%20spokesperson." target="_blank"></a>' +
                            '</li>' +
                            '<li class="popover__nav-socialitem">' +
                            '<a class="share-nav__link share-nav__link--email" href="mailto:?subject=The%20Australian%20Dons&body=Do%20you%20go%20by%20the%20name%20Don,%20Donna%20or%20‘The%20Don’?%20If%20so,%20you%20could%20be%20DON%20Smallgoods%20next%20spokesperson.%0A%0ASign%20up%20today%20for%20your%20shot%20at%20meat%20related%20fame%20and%20glory%20www.australiandons.com.au%0A%0AThe%20DON%20Smallgoods%20team">' +
                            '<span class="shareicon shareicon-email" data-grunticon-embed></span>' +
                            '</a>' +
                            '</li>' +
                            '</ul></nav>',
                        html: true
                    })
                .click(function (e) {
                    e.preventDefault();
                });
            });
    };


    TRIBAL.sharepopover = module;
    // run module
    TRIBAL.sharepopover.init();

}(TRIBAL));