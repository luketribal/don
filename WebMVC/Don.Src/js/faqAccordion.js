/*! faqAccordion */
/***************************************************************************************************************************************************************
 *
 * faqAccordion
 *
 * 
 *
 **************************************************************************************************************************************************************/


(function(TRIBAL) {
	var module = {};
	

	/**
	 * init()
	 * Contructor function for this module
	 */
	module.init = function() {
		var firsttAttrValue = $('.accordion-section-title').first().attr('href');
		$('.accordion-section-title').first().addClass('active');
		$('.accordion ' + firsttAttrValue).slideDown(300).addClass('open'); 


		$('.accordion-section-title').click(function(e) {
			// Grab current anchor value
			var currentAttrValue = $(this).attr('href');

			if($(e.target).is('.active')) {
				close_accordion_section();				
			}else {
				close_accordion_section();

				// Add active class to section title
				$(this).addClass('active');
				// Open up the hidden content panel
				$('.accordion ' + currentAttrValue).clearQueue().stop(true, true).slideDown(300).addClass('open'); 
				
			}
			e.preventDefault();
		});

		
	};

	function close_accordion_section() {
		$('.accordion .accordion-section-title').removeClass('active');
		$('.accordion .accordion-section-content').clearQueue().stop(true, true).slideUp(300).removeClass('open');
	}

	
	

	TRIBAL.faqAccordion = module;
	// run module
	TRIBAL.faqAccordion.init();

}(TRIBAL));