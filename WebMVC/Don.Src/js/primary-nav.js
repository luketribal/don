/*! secondarynav */
/***************************************************************************************************************************************************************
 *
 * secondarynav
 *
 * 
 *
 **************************************************************************************************************************************************************/


(function(TRIBAL) {
	var module = {};
	var $nav = $('.home-nav');
	var anchors = [];

	/**
	 * init()
	 * Contructor function for this module
	 */
	module.init = function() {

		$nav.on('click', '.homeNav-link', function() {
			navigateToAnchor(this.getAttribute('href').replace('#', ''));
			return false;
		});		
	};




	var navigateToAnchor = function(anchor, param) {
		if(!anchor) return;

		var anchor_pos = 0;

		
		if(anchor=='watchTheFilm') {
			anchor_pos = $('#watchTheFilm').offset().top;			
		}
		if(anchor=='registerForm') {
			anchor_pos = $('#registerForm').offset().top;
		}

		console.log(anchor_pos);
		$('html, body').animate({
			scrollTop: anchor_pos
		}, 750, 'easeOutQuint', function() {
			window.history.replaceState({}, null, '#'+anchor);
		});

	};
	

	TRIBAL.secondarynav = module;
	// run module
	TRIBAL.secondarynav.init();

}(TRIBAL));