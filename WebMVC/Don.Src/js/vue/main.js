import Vue from 'vue'
import app from './components/app.vue'

window.application = new Vue({
    el: '#app-container',
    components: {app}
})

