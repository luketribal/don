/*! samplemodule v1.0.0 */
/***************************************************************************************************************************************************************
 *
 * samplemodule
 *
 **************************************************************************************************************************************************************/


(function(TRIBAL) {
	var module = {};

	/**
	 * init()
	 * Contructor function for this module
	 */
	module.init = function() {
	};


	/**
	 * privateMethod()
	 * Example private method
	 */
	var privateMethod = function() {
		return true;
	};


	/**
	 * publicMethod()
	 * Example public method
	 */
	module.publicMethod = function() {
		return true;
	};


	TRIBAL.samplemodule = module;
	TRIBAL.samplemodule.init();
}(TRIBAL));