/*! ShareNav */
/***************************************************************************************************************************************************************
 *
 * ShareNav
 *
 * 
 *
 **************************************************************************************************************************************************************/


(function(TRIBAL) {
	var module = {};
	var $sharenav = $('.share-nav');
	

	/**
	 * init()
	 * Contructor function for this module
	 */
	module.init = function() {
		if(!$sharenav.length) return;

		setShareUrls();

		$sharenav.on('click', '.share-nav__link--top', function() {
			toTop();
			return false;
		});
	};
	
	var setShareUrls = function() {
		var $items =  $sharenav.find('.share-nav__link');

		$items.each(function() {
			var $this = $(this);
			var href = $this.attr('href');
			$this.attr('href', href.replace('{{page_share_url}}', document.location.href));
		});
	};
	
	var toTop = function() {
		$('html, body').animate({
			scrollTop: 0
		}, 750, 'easeOutQuint');
	}

	TRIBAL.ShareNav = module;
	// run module
	TRIBAL.ShareNav.init();

}(TRIBAL));