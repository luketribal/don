/*! watchfilm */
/***************************************************************************************************************************************************************
 *
 * watchfilm
 *
 * 
 *
 **************************************************************************************************************************************************************/

(function (TRIBAL) {
	var module = {};
	var $region = $('.region-banner');

	/**
	 * init()
	 * Contructor function for this module
	 */
	module.init = function () {
		const getVideoLink = idx => {
			const currentSlide = $('.carousel-slide').get(idx);
			return $(currentSlide).data('videolink');
		}
		const carousel$ = Rx.Observable.just('init slick carousel').subscribe(val => {
			$('.carousel-slides').slick();
		});

		// const slideNav$ = Rx.Observable.fromEvent(document.querySelector('button.slick-next'), 'click')
		// 	.merge(Rx.Observable.fromEvent(document.querySelector('button.slick-prev'), 'click'))
		// 	.merge(Rx.Observable.fromEvent($('.carousel-slides'), 'swipe'))
		// 	.delay(500);

		const slideNav$ = Rx.Observable.fromEvent($('.carousel-slides'), 'afterChange');

		const video$ = slideNav$.map(() => {
			const idx = $('.carousel-slides').slick('slickCurrentSlide');
			return getVideoLink(idx);
		})
			.startWith(getVideoLink(0));

		const clickPlay$ = Rx.Observable.fromEvent($('.video-play'), 'click')
			.merge(slideNav$.map(e => {
				return null
			}))
			.withLatestFrom(video$)
			.map(([event, video]) => {
				return { link: video, target: event != null ? event.target : null }
			});

		clickPlay$.subscribe(val => {
			if (!val.target) {
				$('.cmp-playvideo-placeholder').show();
				$('.cmp-playvideo').html('');
			} else {

				if ($("html").hasClass("ie8") || TRIBAL.breakpoints.is('xs')) {
					window.open(`${val.link}?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1`, '_blank');
				}
				else {
					$(val.target).closest('.cmp-playvideo-placeholder').hide();
					$(val.target).closest('.cmp-playvideo-placeholder').siblings('.cmp-playvideo').html(`<div class="responsive-videos"><iframe class="responsive-video" width="560" height="315" src="${val.link}?rel=0&amp;controls=0&amp;showinfo=0&autoplay=1" frameborder="0" allowfullscreen></iframe></div>`).css('background', 'none');
				}
			}
		});

	};


	TRIBAL.watchfilm = module;
	// run module
	TRIBAL.watchfilm.init();

}(TRIBAL));