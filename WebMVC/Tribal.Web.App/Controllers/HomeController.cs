﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;
using Tribal.Web.App.Factories;
using Tribal.Web.App.Models;
using Tribal.Web.DAL.Domain;
using Tribal.Web.DAL.Service;

namespace Tribal.Web.App.Controllers
{
    public class HomeController : Controller
    {
        //public CandidateService Service { get; set; }
        private CandidateService _service;

        private string[] allowedMimeType = {
            "image/jpeg",
            "image/png"
        };

        private const int MAX_FILE_SIZE = 2097152;

        public HomeController(CandidateService service)
        {
            _service = service;
        }

        public ActionResult Faqs()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View("Privacy", null, "Privacy");
        }

        public ActionResult TermsAndConditions()
        {
            return View("TermsAndConditions", null, "Terms & Condition");
        }

        // GET: Home
        public ActionResult Index()
        {
            RestoreViewBag();

            var model = new CandidateViewModel();
            return View(model);
        }        

        [HttpPost]
        public async Task<ActionResult> Index(CandidateViewModel model)
        {
            ViewBag.FacebookKey = ConfigurationManager.AppSettings["FacebookApiKey"];
            ViewBag.Years = _service.PopulateYearRange().Select(p => new SelectListItem
            {
                Text = p,
                Value = p
            }).ToList();

            ViewBag.Products = _service.GetAllProducts()
               .Select(p => new SelectListItem
               {
                   Text = p.ProductName,
                   Value = p.ProductId.ToString()
               }).ToList();

            var fileError = false;
            var hasValidFile = true;
            if (ModelState.IsValid)
            {
                if (!await _service.VerifyCandidateEmail(model.Email))
                {
                    ModelState.AddModelError("Email", "Email has been used.");
                }

                //check if any new file uploads
                var file = Request.Files[0];
                if (file == null || file.ContentLength == 0)
                {
                    hasValidFile = false;
                    if (model.Photo == null || model.Photo.Length == 0)
                    {
                        fileError = true;
                        ModelState.AddModelError("Photo", "Please upload your photo");
                    }
                }
                else
                {
                    model.Photo = null;
                    model.Filename = null;

                    //invalid photo type
                    if (allowedMimeType.All(p => !p.Equals(file.ContentType, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        hasValidFile = false;
                        fileError = true;
                        ModelState.AddModelError("Photo", "Please upload .jpeg/.jpg/.png file");
                    }

                    if (file.ContentLength > MAX_FILE_SIZE)
                    {
                        hasValidFile = false;
                        fileError = true;
                        ModelState.AddModelError("Photo", "File is too big. Max size is 2MB.");
                    }
                }
            }

            if (!fileError && hasValidFile)
            {
                var photo = _service.ConvertCandidatePhoto(Request.Files[0]);
                model.Photo = photo?.ImageContent;
                model.Filename = photo?.Filename;
            }

            if (ModelState.IsValid)
            {
                var entity = model.ToEntity();
                var result = await _service.SaveCandidate(entity);
                #region send email
                var emailFactory = new EmailerFactory();
                var subject = "Thank you for registering to be the next DON Smallgoods spokesperson.";
                var url = Request.IsLocal
                    ? string.Format($"{Request?.Url?.Scheme}://localhost:{Request?.Url?.Port}")
                    : string.Format($"{Request?.Url?.Scheme}://{Request?.Url?.Host}");
                HostingEnvironment.QueueBackgroundWorkItem(_ => emailFactory.SendAsync(model.Email, subject, "Submit", new Dictionary<string, object>
                {
                    {"url", url}
                }));
                #endregion
                if (result) return RedirectToAction("Thankyou");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Thankyou()
        {
            return Redirect("/");
        }

        private void RestoreViewBag()
        {
            ViewBag.FacebookKey = ConfigurationManager.AppSettings["FacebookApiKey"];
            ViewBag.Years = _service.PopulateYearRange().Select(p => new SelectListItem
            {
                Text = p,
                Value = p
            }).ToList();

            ViewBag.Products = _service.GetAllProducts()
                .Select(p => new SelectListItem
                {
                    Text = p.ProductName,
                    Value = p.ProductId.ToString()
                }).ToList();
        }
    }
}