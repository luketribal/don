﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Ionic.Zip;
using Tribal.Web.App.Models;
using Tribal.Web.DAL.Domain;
using Tribal.Web.DAL.Service;

namespace Tribal.Web.App.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        //public CandidateService Service { get; set; }
        private CandidateService _service;

        public AdminController(CandidateService service)
        {
            _service = service;
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public JsonResult GetCandidates()
        {
            var candidates = _service.GetAll<Candidate>().Select(p => new CandidateViewModel
            {
                CandidateId = p.Id,
                Firstname = p.Firstname,
                Lastname = p.Lastname,
                Nickname = p.Nickname,
                Email = p.Email,
                Address = p.Address,
                YearBirth = p.Dob.Year,
                MonthBirth = p.Dob.Month,
                DayBirth = p.Dob.Day,
                CreatedDate = p.Created
            }).ToList();
            return Json(candidates, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetCandidateDetail(Guid candidateId)
        {
            var candidate = await _service.GetCandidateById(candidateId);
            var result = new JsonResult
            {
                Data = new
                {
                    Fullname = string.Format($"{candidate.Firstname} {candidate.Lastname}"),
                    candidate.Nickname,
                    candidate.Email,
                    candidate.Address,
                    YearBirth = candidate.Dob.Year,
                    candidate.Reason,
                    FavoriteProductName = candidate.FavouriteProduct.ProductName,
                    Photo = Convert.ToBase64String(candidate.CandidatePhotos?.FirstOrDefault()?.ImageContent)
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
            return result;
        }

        [HttpGet]
        public async Task<ActionResult> Download()
        {
            var content = await _service.ExportCandidatesToCsv();
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "candidates.csv",

                // always prompt the user for downloading, set to true if you want 
                // the browser to try to show the file inline
                Inline = false,
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(content, "text/csv");
        }

        [HttpGet]
        public async Task<ActionResult> DownloadImages()
        {
            var photos = await _service.GetCandidatePhotos();
            using (var zipFile = new ZipFile())
            {
                photos.Where(p=>!string.IsNullOrEmpty(p.Filename)).ToList().ForEach(photo =>
                {
                    var candidate = $"{photo.Candidate.Firstname}_{photo.Candidate.Lastname}";
                    var extension = Path.GetExtension(photo.Filename);
                    var entryName = $"{candidate}.{extension}";
                    if (zipFile.ContainsEntry(entryName)) entryName = $"{photo.CandidateId}-{entryName}";                    
                    zipFile.AddEntry($"{entryName}", photo.ImageContent);
                });
                zipFile.Save(Server.MapPath("~/DownloadedImages/CandidatePhoto.zip"));
                return File(Server.MapPath("~/DownloadedImages/CandidatePhoto.zip"), "application/zip", $"CandidatePhoto-{DateTime.Now:d}.zip");
            }
        }
    }
}