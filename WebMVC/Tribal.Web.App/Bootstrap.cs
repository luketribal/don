﻿using System;
using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Tribal.Web.App.Factories;

namespace Tribal.Web.App
{
    public class Bootstrap
    {

        private static readonly Lazy<IWindsorContainer> container = new Lazy<IWindsorContainer>(() =>
        {
            var core = new DAL.ComponentInstaller("AustraliaDon");

            var container = new WindsorContainer()
                 .Install(FromAssembly.This())
                 .Install(core);

            return container;
        });

        private Bootstrap()
        {
        }

        public static IWindsorContainer Container
        {
            get
            {
                return container.Value;
            }
        }

        public static void Register()
        {
            var controllerFactory = new ControllerFactory(Container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }
    }
}
