﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tribal.Web.DAL.Domain;

namespace Tribal.Web.App.Models
{
    public class CandidateViewModel
    {
        public Guid CandidateId { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Lastname { get; set; }

        public string Nickname { get; set; }

        public string Fullname => string.Format($"{Firstname} {Lastname}");

        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        public int YearBirth { get; set; }

        [Required(ErrorMessage = "Required")]
        public int MonthBirth { get; set; }

        [Required(ErrorMessage = "Required")]
        public int DayBirth { get; set; }

        [Required(ErrorMessage = "Required")]
        public Guid FavouriteProduct { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Required")]
        public string Reason { get; set; }

        public string Filename { get; set; }

        public byte[] Photo { get; set; }
        

        [Range(typeof(bool), "true", "true", ErrorMessage = "Required")]
        public bool HasDonPermission { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Required")]
        public bool AcceptTermCondition { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Required")]
        public bool ParentConsent { get; set; }     

        public Candidate ToEntity()
        {
            var entity = new Candidate
            {
                Firstname = Firstname,
                Lastname = Lastname,
                Nickname = Nickname,
                Email = Email,
                Address = Address,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                FavouriteProductId = FavouriteProduct,
                Reason = Reason,
                Dob = new DateTime(YearBirth,MonthBirth,DayBirth),
                CandidatePhotos = new List<CandidatePhoto>
                {
                    new CandidatePhoto
                    {
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        Filename = Filename,
                        ImageContent = Photo
                    }
                }
            };
            return entity;
        }
        
        public DateTime CreatedDate { get; set; }
    }
}
