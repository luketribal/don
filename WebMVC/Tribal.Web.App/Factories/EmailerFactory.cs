﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Tribal.Core.Extension;
using Tribal.Core.Helper;
using Tribal.Core.Service;

namespace Tribal.Web.App.Factories
{
    public class EmailerFactory : Emailer, IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {

            TemplateProperties prop;
            var email = message.ToEmail(out prop);
            string body = (prop != null) ? Template(prop.Name, prop.Hash) : message.Body;
            await SendEmailAsync(email, body);

        }

        public async Task SendAsync(string toAddress, string subject, string template, Dictionary<string, object> hash)
        {
            var email = new Email(toAddress, subject);            

            string body = Template(template, hash);
            await SendEmailAsync(email, body);

        }

        public static string Template(string template, IDictionary<string, object> data)
        {
            try
            {
                var renderer = new EmbeddedRenderer(Assembly.GetExecutingAssembly(), "Tribal.Web.App.Templates");
                var tpl = renderer.Render($"{template}.html", data);

                return tpl;
            }
            catch
            {
                return null;
            }
        }

    }
}
