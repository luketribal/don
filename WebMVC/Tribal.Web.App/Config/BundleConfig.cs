﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace Tribal.Web.App.Config
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/rxjs").Include("~/Scripts/rx.js", "~/Scripts/rx.compat.js", "~/Scripts/rx.async.compat.js", "~/Scripts/rx.async.js", "~/Scripts/rx.binding.js", "~/Scripts/rx.lite.compat.js", "~/Scripts/rx.lite.extras.js", "~/Scripts/rx.lite.js", "~/Scripts/rx.aggregates.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/slick-carousel").Include("~/Content/Slick/slick.css", "~/Content/Slick/slick-theme.css"));
            bundles.Add(new ScriptBundle("~/bundles/slick-carousel").Include("~/Scripts/Slick/slick.js"));
        }
    }
}
