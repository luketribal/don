module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		dist: 'Client.Project/',
		src: 'Don.Src/',
		tmp: 'tmp/',
		//CLEAN
		clean: {
			options: {
				force: true
			},
			dist: {
				src: ['<%= dist %>assets/**', '!<%= dist %>assets/js/bundle.js'],
				filter: 'isFile'
			},
			tmp: ['<%= tmp %>']
		},

		//TRANSPILE JS FILES
		babel: {
			options: {
				sourceMap: true,
				presets: ['es2015']				
			},
			dist: {
				files: [{
					expand: true,
					cwd: '<%= src %>js/',
					src: ['**/*.js', '!vue/**'],
					dest: '<%= tmp %>js/modules',
					ext: '-compiled.js'					
				}]
			}
		},


		//COPY TASKS
		copy: {
			html: {
				files: [
					{
						cwd: '<%= src %>html/',
						expand: true,
						flatten: false,
						src: '**/*',
						dest: '<%= dist %>',
						filter: 'isFile'
					}
				]
			},
			images: {
				files: [
					{
						cwd: '<%= src %>images/',
						expand: true,
						flatten: false,
						src: '**/*',
						dest: '<%= dist %>assets/images/',
						filter: 'isFile'
					}
				]
			},
			fonts: {
				files: [
					{
						cwd: '<%= src %>fonts/',
						expand: true,
						flatten: false,
						src: '**/*',
						dest: '<%= dist %>assets/fonts/',
						filter: 'isFile'
					}
				]
			},
		},

		//GENERATE CSS FROM SASS
		sass: {
			development: {
				options: {
					sourcemap: 'none',
					style: 'nested',
					noCache: true,
				},
				files: {
					'<%= dist %>assets/css/style.css': '<%= src %>sass/style.scss'
				}
			},

			release: {
				options: {
					sourcemap: 'none',
					style: 'compressed',
					noCache: true
				},
				files: {
					'<%= dist %>assets/css/style.css': '<%= src %>sass/style.scss'
				}
			}
		},


		//POSTCSS
		postcss: {
			options: {
				processors: [
					require('autoprefixer')({ browsers: ['last 2 versions', 'ie 8', 'ie 9'] }),
				]
			},
			dist: {
				src: '<%= dist %>assets/css/style.css'
			}
		},


		//IMAGE COMPRESSION
		imagemin: {
			release: {
				files: [{
					expand: true,
					cwd: '<%= src %>images/',
					src: ['**/*.{png,jpg,gif,ico,svg}'],
					dest: '<%= dist %>assets/images',
				}]
			}
		},


		//UGLIFY JS (from the TMP)
		uglify: {
			development: {
				options: {
					beautify: {
						beautify: true,
						quote_keys: true
					},
					compress: false,
					mangle: false,
					sourceMap: false
				},
				files: {
					'<%= dist %>assets/js/script.min.js': ['<%= tmp %>js/modules/**/*-compiled.js', '!<%= src %>js/samplemodule.js', '!<%= src %>js/vue/**']
				}
			},

			release: {
				options: {
					beautify: {
						beautify: false,
						quote_keys: true
					},
					compress: true,
					mangle: false,
					sourceMap: false
				},
				files: {
					'<%= dist %>assets/js/script.min.js': ['<%= tmp %>js/modules/**/*-compiled.js', '!<%= src %>js/samplemodule.js', '!<%= src %>js/vue/**']
				}
			}
		},


		//WATCH FOR FILE CHANGES
		watch: {
			styles: {
				files: ['<%= src %>sass/**/*.scss'],
				tasks: ['sass:development', 'postcss'],
				options: {
					livereload: true,
				}
			},
			js: {
				files: ['<%= src %>js/**/*.js', '!<%= src %>js/vue/**'],
				tasks: ['clean:tmp','babel','uglify:development','clean:tmp'],
				options: {
					livereload: true,
				}
			},
			html: {
				files: ['<%= src %>html/**/*.html'],
				tasks: ['copy:html'],
				options: {
					livereload: true,
				}
			},
			images: {
				files: ['<%= src %>images/**/*.{png,jpg,gif,ico,svg}'],
				tasks: ['clean:images', 'copy:images'],
				options: {
					livereload: true,
				}
			}
		},
	});


	//Load Tasks
	grunt.loadNpmTasks("grunt-contrib-clean");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-imagemin");
	grunt.loadNpmTasks("grunt-postcss");
	grunt.loadNpmTasks('grunt-babel');
	grunt.loadNpmTasks("grunt-contrib-uglify");

	grunt.registerTask('dev', [
		'clean:dist',
		'clean:tmp',
		'copy:html',
		'copy:images',
		'copy:fonts',
		'sass:development',
		'postcss',
		'babel',
		'uglify:development',
		'clean:tmp',
		'watch'
	]);

	grunt.registerTask('release', [
		'clean:dist',
		'clean:tmp',
		'copy:html',
		'copy:fonts',
		'imagemin:release',
		'sass:release',
		'postcss',
		'babel',
		'uglify:release',
		'clean:tmp'
	]);

	grunt.registerTask('default', ['release']);
};

