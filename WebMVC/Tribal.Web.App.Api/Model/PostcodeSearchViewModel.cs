﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tribal.Web.App.Api.Model
{
    public class SearchResult
    {
        public Localities localities { get; set; }
    }

    public class Localities
    {
        public List<Locality> locality { get; set; }
    }

    public class Locality
    {
        public string category { get; set; }
        public int id { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string postcode { get; set; }
        public string location { get; set; }
        public string state { get; set; }
    }    
}
