﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using Castle.Components.DictionaryAdapter;
using Newtonsoft.Json;
using RestSharp;
using Tribal.Web.App.Api.Model;
using Tribal.Web.App.Models;
using Tribal.Web.DAL.Domain;
using Tribal.Web.DAL.Service;
using Tribal.Web.Repository;

namespace Tribal.Web.App.Api.Controllers
{
    public class ValuesController : ApiController
    {        
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        public List<Locality> SearchPostcode(string query)
        {
            var baseUrl = ConfigurationManager.AppSettings["AusPostBaseUrl"];
            var resource = ConfigurationManager.AppSettings["AusPostResource"];
            var apiKey = ConfigurationManager.AppSettings["AusPostApiKey"];

            var client = new RestClient(baseUrl);
            var request = new RestRequest($"{resource}?q={query}", Method.GET);
            request.AddHeader("AUTH-KEY", apiKey);

            var result = client.Execute<SearchResult>(request).Data;                        
            return result?.localities?.locality;
        }        
    }
}
