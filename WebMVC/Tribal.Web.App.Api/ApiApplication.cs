﻿using System.Web.Http;
using Tribal.Web.App.Api.Config;

namespace Tribal.Web.App.Api
{
    public class ApiApplication : Application
    {
        public static void Register()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        public override void Application_Start()
        {
            Register();
            base.Application_Start();
        }
    }
}
