﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Tribal.Core.Infrastructure;
using Tribal.Core.Repository;
using Tribal.Core.Service;
using Tribal.Web.Repository;

namespace Tribal.Web.DAL
{
    public class ComponentInstaller: IWindsorInstaller
    {
        private string _connectionString = "AustraliaDon";

        public ComponentInstaller(string connectionString)
        {
            _connectionString = connectionString;
        }
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<EntityDbContext>()                    
                    .DependsOn(Dependency.OnValue<string>("AustraliaDon"))
                    .LifestyleTransient());

            container.Register(Component.For<IContext>().ImplementedBy<EntityRepository>().LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUserRole>()
              .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUserLogin>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUserClaim>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationRole>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUser>()
               .LifestylePerWebRequest());            

            container.Register(Classes.FromThisAssembly()
                .BasedOn<EntityService>()
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName)));
        }
    }
}
