﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribal.Core.Entity;

namespace Tribal.Web.DAL.Domain
{
    public class CandidatePhoto : Entity<Guid>
    {
        public string Filename { get; set; }

        public byte[] ImageContent { get; set; }

        public Guid CandidateId { get; set; }
        
        public virtual Candidate Candidate { get; set; }
    }
}
