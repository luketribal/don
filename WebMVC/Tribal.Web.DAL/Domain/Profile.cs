﻿using System;
using Tribal.Core.Entity;

namespace Tribal.Web.Domain
{
    public class Profile : Entity<Guid>
    {
        public string Name { get; set; }

    }
}
