﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribal.Core.Entity;

namespace Tribal.Web.DAL.Domain
{
    public class Candidate : Entity<Guid>
    {
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Nickname { get; set; }

        public string Email { get; set; }

        public DateTime Dob { get; set; }

        [ForeignKey("FavouriteProduct")]
        public Guid FavouriteProductId { get; set; }

        public string Address { get; set; }

        public string Reason { get; set; }

        public virtual ICollection<CandidatePhoto> CandidatePhotos { get; set; }
        public Product FavouriteProduct { get; set; }
    }
}
