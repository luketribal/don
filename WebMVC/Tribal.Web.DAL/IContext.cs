﻿using System.Data.Entity;
using Tribal.Web.DAL.Domain;
using Tribal.Web.Domain;

namespace Tribal.Web.DAL
{
    public interface IContext
    {
        DbSet<Profile> Profiles { get; set; }

        DbSet<Candidate> Candidates { get; set; }
        DbSet<CandidatePhoto> CandidatePhotos { get; set; }
        DbSet<Product> Products { get; set; }
    }
}