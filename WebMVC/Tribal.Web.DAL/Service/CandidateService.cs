﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CsvHelper;
using Tribal.Core.Service;
using Tribal.Web.DAL.Domain;
using Tribal.Web.Repository;

namespace Tribal.Web.DAL.Service
{
    public class CandidateService : EntityService
    {
        public IContext DbContext { get; set; }
        public CandidateService() : base(new EntityRepository())
        {

        }

        public CandidateService(EntityRepository context) : base(context)
        {

        }

        /// <summary>
        /// Generate year of birth range
        /// </summary>
        /// <returns></returns>
        public List<string> PopulateYearRange()
        {
            var youngest = DateTime.Now.Year - 10;
            var oldest = DateTime.Now.Year - 100;

            return Enumerable.Range(oldest, youngest - oldest).Select(p => p.ToString()).OrderByDescending(p => p).ToList();
        }

        public List<Product> GetAllProducts()
        {
            return GetAll<Product>().ToList();
        }

        public async Task<bool> SaveCandidate(Candidate tobeSaved)
        {
            try
            {
                ((EntityRepository)_dbContext).Candidates.Add(tobeSaved);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<Candidate> GetCandidateById(Guid candidateId)
        {
            try
            {
                var result =
                    await ((EntityRepository)_dbContext).Candidates.Include(p => p.FavouriteProduct).Include(p => p.CandidatePhotos).FirstOrDefaultAsync(p => p.Id == candidateId);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public CandidatePhoto ConvertCandidatePhoto(HttpPostedFileBase file)
        {
            byte[] data;
            using (Stream inputStream = file.InputStream)
            {
                MemoryStream memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = memoryStream.ToArray();
            }
            var photo = new CandidatePhoto
            {
                Filename = file.FileName,
                ImageContent = data
            };
            return photo;
        }

        /// <summary>
        /// verify wether the email is unique or not
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<bool> VerifyCandidateEmail(string email)
        {
            var result =
                await ((EntityRepository)_dbContext).Candidates.AllAsync(p => !p.Email.Equals(email));
            return result;
        }

        public async Task<byte[]> ExportCandidatesToCsv()
        {            
            var candidates =
                await ((EntityRepository) _dbContext).Candidates.Include(p => p.FavouriteProduct).ToListAsync();


            var result = candidates.Select(p => new
            {
                Fullname = string.Format($"{p.Firstname} {p.Lastname}"),
                p.Nickname,
                p.Email,
                Dob = p.Dob.ToString("d"),
                p.Address,
                p.FavouriteProduct.ProductName,
                p.Reason,
                CreatedDate = p.Created.ToString("f")
            }).ToList();

            using (var stream = new MemoryStream())
            {
                using (var sw = new StreamWriter(stream))
                {
                    using (var csv = new CsvWriter(sw))
                    {
                        csv.WriteRecords(result);
                        return stream.ToArray();
                    }                                                            
                }
            }
            
        }

        public async Task<List<CandidatePhoto>> GetCandidatePhotos()
        {            
            var photos = await DbContext.CandidatePhotos.Include(p => p.Candidate).ToListAsync();
            return photos;
        }
    }
}
