﻿using Tribal.Core.Service;

namespace Tribal.Web.Services
{
    using Repository;

    public class AccountService : EntityService
    {
        public AccountService(EntityRepository entityDbContext) : base(entityDbContext) { }
    }
}
