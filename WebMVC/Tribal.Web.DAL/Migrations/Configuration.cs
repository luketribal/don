using Tribal.Web.DAL.Domain;

namespace Tribal.Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Repository.EntityRepository>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Repository.EntityRepository context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Products.AddOrUpdate(p => p.ProductName, new Product
            {
                ProductName = "DON Bacon"
            },
                new Product
                {
                    ProductName = "DON Ham"
                },
                new Product
                {
                    ProductName = "DON Frankfurts"
                },
                new Product
                {
                    ProductName = "DON Salami"
                },
                new Product
                {
                    ProductName = "DON Kabana"
                }, 
                new Product
                {
                    ProductName = "DON Continentals"
                }, 
                new Product
                {
                    ProductName = "DON Luncheon"
                });
        }
    }
}
