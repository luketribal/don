namespace Tribal.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeYearBirthColumn : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Candidate", "YearBirth");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Candidate", "YearBirth", c => c.String());
        }
    }
}
