namespace Tribal.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Adddob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidate", "Dob", c => c.DateTime(nullable: true));
            Sql(@"UPDATE dbo.Candidate SET Dob = CAST(CAST(YearBirth AS varchar) + '-' + CAST(1 AS varchar) + '-' + CAST(1 AS varchar) AS DATETIME)");
            AlterColumn("dbo.Candidate", "Dob", c => c.DateTime(nullable: false));
            DropColumn("dbo.Candidate", "YearBirth");
        }

        public override void Down()
        {
            AddColumn("dbo.Candidate", "YearBirth", c => c.String());
            DropColumn("dbo.Candidate", "Dob");
        }
    }
}
