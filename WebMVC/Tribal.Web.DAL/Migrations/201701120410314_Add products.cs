namespace Tribal.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addproducts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductId = c.Guid(nullable: false, identity: true),
                        ProductName = c.String(),
                    })
                .PrimaryKey(t => t.ProductId);
            
            AddColumn("dbo.Candidate", "FavouriteProductId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Candidate", "FavouriteProductId");
            AddForeignKey("dbo.Candidate", "FavouriteProductId", "dbo.Product", "ProductId", cascadeDelete: true);
            DropColumn("dbo.Candidate", "FavouriteProduct");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Candidate", "FavouriteProduct", c => c.String());
            DropForeignKey("dbo.Candidate", "FavouriteProductId", "dbo.Product");
            DropIndex("dbo.Candidate", new[] { "FavouriteProductId" });
            DropColumn("dbo.Candidate", "FavouriteProductId");
            DropTable("dbo.Product");
        }
    }
}
