namespace Tribal.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addfirstnamelastnamenickname : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidate", "Firstname", c => c.String());
            AddColumn("dbo.Candidate", "Lastname", c => c.String());
            AddColumn("dbo.Candidate", "Nickname", c => c.String());
            AddColumn("dbo.Candidate", "YearBirth", c => c.String());
            DropColumn("dbo.Candidate", "Fullname");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Candidate", "Fullname", c => c.String());
            DropColumn("dbo.Candidate", "YearBirth");
            DropColumn("dbo.Candidate", "Nickname");
            DropColumn("dbo.Candidate", "Lastname");
            DropColumn("dbo.Candidate", "Firstname");
        }
    }
}
