namespace Tribal.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCandidatePhotos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CandidatePhoto",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Filename = c.String(),
                        ImageContent = c.Binary(),
                        CandidateId = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidate", t => t.CandidateId, cascadeDelete: true)
                .Index(t => t.CandidateId);
            
            CreateTable(
                "dbo.Candidate",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Fullname = c.String(),
                        Email = c.String(),
                        YearBirth = c.String(),
                        FavouriteProduct = c.String(),
                        Address = c.String(),
                        Reason = c.String(),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CandidatePhoto", "CandidateId", "dbo.Candidate");
            DropIndex("dbo.CandidatePhoto", new[] { "CandidateId" });
            DropTable("dbo.Candidate");
            DropTable("dbo.CandidatePhoto");
        }
    }
}
