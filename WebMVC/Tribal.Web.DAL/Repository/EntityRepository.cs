﻿using System;
using System.Data.Entity;
using Tribal.Web.DAL;
using Tribal.Web.DAL.Domain;

namespace Tribal.Web.Repository
{

    using Core.Repository;
    using Domain;

    public class EntityRepository : EntityDbContext, IContext
    {
        public EntityRepository() : base("AustraliaDon")
        {
            
        }
        public DbSet<Profile> Profiles { get; set; }

        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<CandidatePhoto> CandidatePhotos { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
